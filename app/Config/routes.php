<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/View/Pages/home.ctp)...
 */
        Router::connect('/', array('controller' => 'Home', 'action' => 'home'));
        
        Router::connect('/products.htm', array('controller' => 'Home', 'action' => 'home'));

        Router::connect('/εταιρεία', array('controller' => 'About', 'action' => 'about'));
        Router::connect('/επικοινωνία', array('controller' => 'About', 'action' => 'contact'));
        Router::connect('/service', array('controller' => 'About', 'action' => 'service'));
        Router::connect('/σιδερώστρες', array('controller' => 'products', 'action' => 'ironing_board_list'));
        Router::connect('/εκθέσεις', array('controller' => 'Exibitions'));

        Router::connect('/iron', array('controller' => 'admin', 'action' => 'login'));
        
        Router::connect('/:category', array('controller' => 'products', 'action' => 'productList'),
                array('pass' => array('category')));
        Router::connect('/ατμοκαθαριστές/:id', array('controller' => 'products', 'action' => 'product_details'),
                array('pass' => array('id')));        
        Router::connect('/οικολογική-απολύμανση/:id', array('controller' => 'products', 'action' => 'product_details'),
                array('pass' => array('id')));  
        Router::connect('/πολυσκούπες/:id', array('controller' => 'products', 'action' => 'product_details'),
                array('pass' => array('id')));  
        Router::connect('/σκούπες/:id', array('controller' => 'products', 'action' => 'product_details'),
                array('pass' => array('id')));  
        Router::connect('/unico-polti/:id', array('controller' => 'products', 'action' => 'unico_details'),
                array('pass' => array('id')));          
        Router::connect('/πρεσσοσίδερα/:id', array('controller' => 'products', 'action' => 'product_details'),
                array('pass' => array('id')));          
        Router::connect('/σιδερώστρες/:id', array('controller' => 'products', 'action' => 'ironing_board_details'),
                array('pass' => array('id')));                  

        
 /** 
 * ...and connect the rest of 'Pages' controller's URLs.
 */
	Router::connect('/pages/*', array('controller' => 'pages', 'action' => 'display'));

/**
 * Load all plugin routes. See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
	CakePlugin::routes();

/**
 * Load the CakePHP default routes. Only remove this if you do not want to use
 * the built-in default routes.
 */
	require CAKE . 'Config' . DS . 'routes.php';