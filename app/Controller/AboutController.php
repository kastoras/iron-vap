<?php

    class AboutController extends AppController{

        public function about(){
            $this->set('title_for_layout', 'Iron Vap | Στοιχεία εταιρίας');
            $this->set('description_for_layout', 'Η Iron Vap με έδρα τη Θεσσαλονίκη εμπορεύεται ατμοκαθαριστές, πολυσκούπες, σκούπες & συστήματα απολύμανσης σε όλη την Ελλάδα');
        }
        
        public function contact(){
            $this->set('title_for_layout', 'Iron Vap | Φόρμα Επικοινωνίας ');
            $this->set('description_for_layout', 'Βρείτε μας στο χάρτη της Θεσσαλονίκης καθώς και επικοινωνίστε μαζί μας κατευθείαν από τη φόρμα επικοινωνίας');
        }        
        
        public function service(){
            $this->set('title_for_layout', 'Iron Vap | Στοιχεία Service ');
            $this->set('description_for_layout', 'Στοιχεία για για το service στη Θεσσαλονίκη και την Αθήνα');
        }          
        
        public function mailIt(){
            //$this->layout = 'ajax';
            if($this->request->is('post'))
            {
                if (isset($_REQUEST)){
                    
                    $mailTo = "info@iron-vap.gr";
                    if (isset($_REQUEST['name'])){ $mailFromName = $_REQUEST['name']; }
                    if (isset($_REQUEST['mail'])){ $mailFromEmail = $_REQUEST['mail']; }
                    if (isset($_REQUEST['phone'])){ $mailFromPhone = $_REQUEST['phone']; }
                    if (isset($_REQUEST['subject'])){ $subject = $_REQUEST['subject']; }
                    if (isset($_REQUEST['message'])){ $message = $_REQUEST['message']; }
                    $mailFromWebsite = "http://iron-vap.gr/";
               
                    $msg = "This message was send from: $mailFromWebsite \n\nby: $mailFromName \n\nEmail: $mailFromEmail \n\nPhone: $mailFromPhone \n\nSubject: $subject \n\nText of message: $message";
                    $headers = "MIME-Version: 1.0\r\n Content-type: text/html; charset=utf-8\r\n From: $mailFromEmail\r\n Reply-To: $mailFromEmail";
                   
                    mail($mailTo, $subject, $msg, $headers);

                    $this->render('contact');
                }
            }

        }
    }
?>