<?php

    class AdminController extends AppController{
        
        
        public function beforeFilter(){
            parent::beforeFilter();

            if($this->request->action != 'login' && !$this->Session->check('User')){
                $this->Session->setFlash('Please log in first.');
                $this->redirect(array('controller'=>'/'));
            }
        }
        
        public function login(){
            
            $this->layout = 'login';

            if($this->request->is('post')){

                $user = $this->Admin->find('first',array(
                        'recursive' => -1,
                        'conditions'=>array(
                            'username' => $this->request->data('Admin.name'),
                            'pass' => $this->request->data('Admin.password')
                        )
                    )
                );

                if($user){
                    pr($user);
                    $this->Session->write('User',$user);
                    $this->redirect(
                        array(
                            'controller'=>'Products',
                            'action'=>'prices'
                        )
                    );
                }
                else{
                    $this->Session->setFlash('Something went wrong...');
                }

            }
            
        }        
        
        /* No Use*/
        public function dash(){
            $this->layout = 'admin';
        }
       
    }
?>
