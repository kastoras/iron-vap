<?php

    class ExibitionsController extends AppController{

        public function index(){
            $this->set('title_for_layout', 'Iron Vap | Εκθέσεις ');
            $this->set('description_for_layout', 'Συμμετοχή της Iron Vap σε εκθέσεις');
            
            $exibitions = $this->Exibition->getAllExibitions();
            $this->set('exibitions',$exibitions);
        }
        
        public function events($id){
            
            if(!$this->Session->check('User')){
                $this->Session->setFlash('Please log in first.');
                $this->redirect(array('controller'=>'/'));
            }
            
            if(isset($id)){
                $this->Exibition->id = $id;
                $this->Exibition->delete();
                
            }

            $this->layout = 'admin';
            
            $events = $this->Exibition->find('all');
            
            $this->set('events', $events);
        }        
        
        public function newEvent(){

            if(!$this->Session->check('User')){
                $this->Session->setFlash('Please log in first.');
                $this->redirect(array('controller'=>'/'));
            }
            $this->layout = 'admin';  
            
            
            if($this->request->is('post')){
                $this->Exibition->save($this->request->data);
                
                $this->Session->setFlash('Επιτυχής εισαγωγή');      
            }             
                    
        }
        
        public function updateEvent($id){

            if(!$this->Session->check('User')){
                $this->Session->setFlash('Please log in first.');
                $this->redirect(array('controller'=>'/'));
            }

            $this->layout = 'admin';
            
            if($this->request->is('post')){
                $this->Exibition->id = $id;
                $this->Exibition->save($this->request->data);
                
                $this->Session->setFlash('Η αλλαγή εκτελέστηκε!');   
            }            
            
            $event = $this->Exibition->find('first',
                array('conditions'=>array('Exibition.id'=>$id)));
            
            $this->set('event',$event);                
        }

    }
?>