<?php

    class HomeController extends AppController{

        public function home(){
            $this->set('title_for_layout', 'Iron Vap | Εισαγωγές ατμοκαθαριστές σκούπες ατμού');
            $this->set('description_for_layout', 'Εισαγωγές οικιακών συσκευών, ατμοκαθαριστές για βαθύ καθαρισμό χαλιών, απολύμανση/απαλλαγή από ακάρεα|κοριούς');            
            
            $products = $this->Home->getAllHomeProducts();
            $this->set('products', $products);
        }
    }