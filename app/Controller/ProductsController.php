<?php
    class ProductsController extends AppController{
        
        const steam_cleaners = 'Ατμοκαθαριστές';  
        const green_cleaning = 'Οικολογική Απολύμανση';  
        const vacum = 'Πολυσκούπες';  
        const brushers = 'Σκούπες';  
        const unico = 'UNICO POLTI'; 
        const wringer = 'Πρεσσοσίδερα';
        const ironing_board = 'Σιδερώστρες';
        
        public function product_details($id){
            $product = $this->Product->getProduct($id);
            $addives = $this->Product->getProductAddives($product[0]["Product"]["id_code"]);
            $advantages = $this->Product->getProductAdvantages($product[0]["Product"]["id_code"]);
            
            $this->set('title_for_layout', 'Iron Vap | '.$product[0]["Product"]["name"]);
            $this->set('description_for_layout', 'Αναλυτικές πληροφορίες για το '.$product[0]["Product"]["name"]);            
            $this->set('product', $product[0]["Product"]);
            $this->set('additives', $addives);
            $this->set('advantages', $advantages);            
            $this->set('category', $this->Product->mb_ucfirst($product[0]["Product"]["category"],"utf8"));
            $this->set('categoryLink', $product[0]["Product"]["category"]);
        }

         public function productList($category){
            $this->set('title_for_layout', 'Iron Vap | '.$category);
            $this->set('description_for_layout', 'Λίστα με '.$category);           
            
            $product = $this->Product->getAllProductsFromCategory($category);
            $this->set('products', $product);
            $this->set('category', $this->Product->mb_ucfirst($product[0]["Product"]["category"],"utf8"));
        }       
        
        public function unico_details($id){
            $product = $this->Product->getProduct($id);
            $addives = $this->Product->getProductAddives($product[0]["Product"]["id_code"]);
            $advantages = $this->Product->getProductAdvantages($product[0]["Product"]["id_code"]);
            
            $this->set('title_for_layout', 'Iron Vap | Unico');
            $this->set('description_for_layout', 'Λεπτομέριες για το προϊόν '.$product[0]["Product"]["name"]);             
            $this->set('product', $product[0]["Product"]);
            $this->set('additives', $addives);
            $this->set('advantages', $advantages);            
            $this->set('category', $this->Product->mb_ucfirst($product[0]["Product"]["category"],"utf8"));      
            $this->set('category', $this->Product->mb_ucfirst($product[0]["Product"]["category"],"utf8"));
            $this->set('categoryLink', $product[0]["Product"]["category"]);            
        }

        public function ironing_board_list(){
            $product = $this->Product->getAllProductsFromCategory('σιδερώστρες');
            
            $this->set('title_for_layout', 'Iron Vap | Σιδερώστρες');
            $this->set('description_for_layout', 'Λίστα με τις σιδερώστρες της ςταιρίας μας');             
            $this->set('products', $product);
            $this->set('category', $this->Product->mb_ucfirst($product[0]["Product"]["category"],"utf8"));
        }        
        
        public function ironing_board_details($id){
            $this->set('title_for_layout', 'Iron Vap | Σιδερώστρα'.$product[0]["Product"]["name"]);
            $this->set('description_for_layout', 'Πληροφορίες για την σιδερώστρα '.$product[0]["Product"]["name"]); 
            
            $product = $this->Product->getProduct($id);      
            $this->set('product', $product[0]["Product"]);          
            $this->set('category', $this->Product->mb_ucfirst($product[0]["Product"]["category"],"utf8"));            
            $this->set('categoryLink', $product[0]["Product"]["category"]);             
        }
        
        public function prices(){
            
            if(!$this->Session->check('User')){
                $this->Session->setFlash('Please log in first.');
                $this->redirect(array('controller'=>'/'));
            }

            $this->layout = 'admin';
            
            $products = $this->Product->find('all',
                array(
                    'fields' => array('Product.id', 'Product.name', 'Product.price', 'Product.offer'),
                    'order'=>'Product.category'
                )
            );
            
            $this->set('products', $products);
        }        
        
        public function updatePrice($id){
            
            if(!$this->Session->check('User')){
                $this->Session->setFlash('Please log in first.');
                $this->redirect(array('controller'=>'/'));
            }
            
            $this->layout = 'admin';
            
            if($this->request->is('post')){
                
                pr($id);
                pr($this->request->data);
                $this->Product->id = $id;
                $this->Product->save($this->request->data);
                
                $this->Session->setFlash('Η αλλαγή εκτελέστηκε!');
                
                $this->redirect(
                    array(
                        'controller'=>'Products',
                        'action'=>'prices'
                    )
                );
            }
            
            $product = $this->Product->find('first',
                array('conditions'=>array('Product.id'=>$id)));
            
            $this->set('product',$product);
        }
        
    }
?>