<?php
    class Product extends AppModel {
    
        public function getProduct($id){
            $products = array();
            $products = $this->find('all', 
                array('conditions' =>
                    array('id' => $id)
                )
            );
            return $products;
        }
        
        public function getProductAddives($id){
            $addives =  ClassRegistry::init('product_additives')->find('all',
                array('conditions' =>
                    array('product_id' => $id)
                )
            ); 
            return $addives;
        }
        
        public function getProductAdvantages($id){
            $advantage =  ClassRegistry::init('product_advantages')->find('all',
                array('conditions' =>
                    array('product_id' => $id)
                )
            ); 
            return $advantage;            
        }        

        public function getAllProductsFromCategory($category){
            $products = array();
            $products = $this->find('all', 
                array('conditions' =>
                    array('category' => $category),
                    'order'=>'ordering'
                ));
            return $products;
        }

        public function getAllProducts(){
            $products = array();
            $products = $this->find('all');
            return $products;
        }        
        
        public function mb_ucfirst($string, $encoding){
            $strlen = mb_strlen($string, $encoding);
            $firstChar = mb_substr($string, 0, 1, $encoding);
            $then = mb_substr($string, 1, $strlen - 1, $encoding);
            return mb_strtoupper($firstChar, $encoding) . $then;
        }

    }
?>