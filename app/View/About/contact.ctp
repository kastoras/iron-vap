<div class="page_container">
    <h6><a href="">Αρχική</a> / επικοινωνία</h6>
    <div class="inner_content">
        <div class="row">
            <div class="span12">
                <!--//GOOGLE MAP -ADD YOUR EMBED INFO HERE-->
                <div id="map">
                    <iframe height="310" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d757.187744222574!2d22.960849124592755!3d40.61331760377501!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x14a838e73bccd675%3A0xd6b109278acbd26e!2zzprPic69z4PPhM6xzr3PhM65zr3Ov8-Fz4DPjM67zrXPic-CIDY2LCDOmM61z4PPg86xzrvOv869zq_Ous63IDU0NiA0MSwgzpXOu867zqzOtM6x!5e0!3m2!1sel!2s!4v1430122444274" width="600" height="450" frameborder="0" style="border:0"></iframe>
                </div>
            </div>
		
            <div class="span5 contactInfoOuter">
                <div class="contactInfoInner">
                    <h6>Στοιχεία επικοινωνίας</h6>
                    <address>
                        <span class="greenColorContact">Διεύθυνση</span><br>
                        <strong>Κωνσταντινουπόλεως 66,</strong><br>
                        <strong>Έναντι Ιπποκρατείου,</strong><br>
                        <strong>54641</strong><br>
                    </address>
                    <address>
                        <span class="greenColorContact">Τηλέφωνο</span><br>
                        <strong>(0030) 2310 85 30 30</strong>
                    </address>
                    <address>
                        <span class="greenColorContact">Κινητό</span><br>
                        <strong>(0030) 6946124291</strong>
                    </address>           
                    <address>
                        <span class="greenColorContact">Fax</span><br>
                        <strong>(0030) 2310 853850</strong>
                    </address>                
                    <address>
                        <strong>Email</strong><br>
                        <a href="mailto:#" style="font-size:17px"><strong>info@iron-vap.gr</strong></a>
                    </address>


                    <section id="labels">
                    <strong>IBAN</strong><br>
                     <table class="table table-bordered table-striped">
                        <thead>
                          <tr>
                            <th>Τράπεζα</th>
                            <th>Αριθμός λογαριασμού</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td style="background-color: yellow; color: black;">
                              <span><b>Πειραιώς</b></span>
                            </td>
                            <td>
                              GR95 0172 2280 0052 2807 0699 850
                            </td>
                          </tr>                        
                          <tr>
                            <td style="background-color: blue; color: white;">
                              <span><b>Alpha Bank</b></span>
                            </td>
                            <td>
                              GR44 0140 7110 7110 0232 0000 583
                            </td>
                          </tr>
                          <tr>
                            <td style="background-color: aqua; color: white;">
                              <span><b>Εθνική</b></span>
                            </td>
                            <td>
                              GR29 0110 2230 0000 2234 0076 037
                            </td>
                          </tr>
                          <tr>
                            <td>
                              <span><b>Δικαιούχος</b></span>
                            </td>
                            <td>
                              Τσινίδης Ανέστης
                            </td>
                          </tr>                      
                        </tbody>
                      </table>
                    </section>                    
                </div>
            </div>
            <div class="span6">
		<div class="contact_form well">  
                    <div id="note"></div>
                    <div id="fields">
                        <form id="ajax-contact-form" action="About/mailIt" method="post">
                   
                            <p class="form_info">ονομα <span class="required">*</span></p>
                            <input class="span5" type="text" name="name" id="name" value="" />
                            <p class="form_info">email <span class="required">*</span></p>
                            <input class="span5" type="text" id="mail" name="mail" value=""  />
                            <p class="form_info">Τηλέφωνο</p>
                            <input class="span5" type="text" id="phone" name="phone" value=""  />                            
                            <p class="form_info">Θεμα</p>
                            <input class="span5" type="text" id="subject" name="subject" value="" /><br>
                            <p class="form_info">Μηνυμα</p>
                            <textarea name="message" id="message" class="span5" ></textarea>
                            <div class="clear"></div>

                            <input type="submit" class="btn-form" value="αποστολη" />
                            <input type="reset"  class="btn-form" value="καθαρισμος" />
                            <div class="clear"></div>
                        </form>
                    </div>
                </div>                   
            </div>                	
        </div>
    </div>
</div>
</div>