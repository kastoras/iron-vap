<div class="span6 priceHeight">
    <div class="row">
        <div class="span3 pricing-table">
            <ul>
                <li class="pricing-header-row-1 price_header">
                <h4>Τιμή</h4>
                </li>
                <li class="pricing-header-row-2 priceEl">
                    <h4 class="hue price priceSize">
                        <?php if($id == "ατμοκαθαριστής-xg450"): ?>
                            <span style="font-size: 20px;">Μόνο </span>
                        <?php endif; ?>
                        <?php echo $price; ?>€
                    </h4>
                    <p>Συμπεριλαμβάνεται ΦΠΑ 24%.</p>
                </li>
            </ul>    
        </div> 
        <?php if($id == "ατμοκαθαριστής-xg450"): ?>
            <div class="span6 offers_period">
                <p>Μαζί με δώρα αξίας <b>50</b>€!</p>
                <p>Για λίγες ημέρες ακόμα!</p>
            </div>
        <?php endif; ?>     
        <?php if($id == "disinfector" || $id == "cimex-eradicator"): ?>
            <div class="span6 offers_period">
                <p>Περιορισμένος αριθμός τεμαχίων</p>    
            </div>
        <?php endif; ?>     
    </div>
</div>    