<div class="grid_16">
<!-- TABS START -->
    <div id="tabs">
         <div class="container">
            <ul>
                <li>
                    <?php
                    echo $this->Html->link('<span>Λίστα Εκδηλώσεων</span>',
                        array('controller' => 'Exibitions','action' => 'events'),
                        array('escape' => FALSE)
                    );
                    ?>
                </li>
                <li>
                    <?php
                    echo $this->Html->link('<span>Καινούρια Εκδήλωση</span>',
                        array('controller' => 'Exibitions','action' => 'newEvent'),
                        array('escape' => FALSE)
                    );
                    ?>
                </li> 
           </ul>
        </div>
    </div>
<!-- TABS END -->    
</div>
<!-- CONTENT START -->
<div class="grid_16" id="content">
    <?php echo $this->Session->flash(); ?>
    <!--  TITLE START  --> 
    <div class="grid_9">
        <h1 class="dashboard">Διαχείριση Εκθέσεων</h1>
    </div>
    <!--RIGHT TEXT/CALENDAR END-->
    <div class="clear">
    </div>
    <!--  TITLE END  -->    

    <!-- #PORTLETS START -->
    <div id="portlets">
        
    <div class="clear"></div>
    <!--THIS IS A WIDE PORTLET-->
    <div class="portlet">z
        <div class="portlet-content nopadding">
            <form action="" method="post">
                <table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
                    <thead>
                    <tr>
                        <th width="30" scope="col">Α/Α</th>
                        <th width="136" scope="col">Όνομα</th>
                        <th width="102" scope="col">Μέρος</th>
                        <th width="129" scope="col">Αρχή</th>
                        <th width="129" scope="col">Τέλος</th>
                        <th width="129" scope="col">Παραπομπή</th>
                        <th width="129" scope="col">Ενέργειες</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php foreach($events as $key => $event): ?>
                        <tr>
                            <td width="30"><?php echo ($key+1).'.';?></td>
                            <td>
                            <?php
                                echo $event['Exibition']['name'];
                            ?>
                            </td>
                            <td>
                            <?php
                                echo $event['Exibition']['place'];
                            ?>    
                            </td>
                            <td>
                            <?php
                                echo $event['Exibition']['day_start'];
                            ?>    
                            </td>
                            <td>
                            <?php
                                echo $event['Exibition']['day_close'];
                            ?>    
                            </td>                            
                            <td>
                            <?php
                                echo $event['Exibition']['ex_link'];
                            ?>    
                            </td>                             
                            <td width="90">
                                <?php
                                    echo $this->Html->link('',
                                        array('controller' => 'Exibitions','action' => 'updateEvent/'.$event['Exibition']['id']),
                                        array('class'=>'edit_icon','title'=>'Edit')
                                    );
                                ?>
                                <?php
                                    echo $this->Html->link('',
                                        array('controller' => 'Exibitions','action' => 'events/'.$event['Exibition']['id']),
                                        array('class'=>'delete_icon','title'=>'Delete')
                                    );
                                
                                ?>
                            </td>
                        </tr>
                        <?php endforeach ?>
                    </tbody>
                </table>
            </form>
        </div>
    </div>
        <!--  END #PORTLETS -->  
    </div>
</div>    
<div class="clear"> </div>
<!-- END CONTENT-->  