<div class="page_container">
    <h6><?php echo $this->Html->link("Αρχική",'/'); ?> / Εκθέσεις</h6>
    <div class="inner_content">
        <div class="row">
            <div class="row">
		<div class="span12">
                    <div class="post exibitionsHead">
                        <div class="span12">
                            <h2>Συμμετοχή σε εκθέσεις</h2>
                        </div>
                    </div>
                    <?php foreach($exibitions as $exibition): ?>
                    <div class="span12 eventBackground">
                        <div class="post paddingDown5">
                            <div class="span12">
                                <h3><span class="post_link">
                                        <?php echo $exibition['Exibition']['name']; ?>
                                    </span>
                                </h3>

                                <div class="post-meta">
                                    <div class="row">
                                        <?php if($exibition['Exibition']['Image']): ?>
                                        <div class="span2">
                                            <?php echo $this->Html->image($exibition['Exibition']['Image']) ?>
                                        </div>
                                        <?php endif; ?>
                                        <div class="span10">
                                            <div class="row eventPlace paddingDown5 eventsDataList">
                                                <div class="span1 smallerFont">
                                                    Tόπος: 
                                                </div>
                                                <div class="span8 eventsDate">
                                                    <span class="smallerFont"><?php echo $exibition['Exibition']['place']; ?>
                                                </div>
                                            </div>                                        
                                            <div class="row">
                                                <div class="span1 smallerFont eventsFromTo">
                                                    <b>Από:</b> 
                                                </div>
                                                <div class="span2 eventsDate">
                                                    <span class="greenColor smallerFont"><b><?php echo $exibition['Exibition']['day_start']; ?></b></span>
                                                </div>

                                                <div class="span1 smallerFont eventsFromTo">
                                                    <b>Μέχρι:</b> 
                                                </div>
                                                <div class="span2 eventsDate">
                                                    <span class="greenColor smallerFont"><b><?php echo $exibition['Exibition']['day_close']; ?></b></span>
                                                </div>
                                            </div>
                                            <?php if($exibition['Exibition']['Info']): ?>
                                            <div class="row eventPlace">
                                                <div class="span9 eventsDataFont">
                                                    <?php echo $exibition['Exibition']['Info']; ?>
                                                </div>
                                            </div>     
                                            <?php endif; ?>
                                            <div class="row eventPlace">
                                                <div class="span3 eventsDataFont">
                                                <?php
                                                    if($exibition['Exibition']['ex_link']){
                                                        echo $this->Html->link('Περισσότερες πληροφορίες', $exibition['Exibition']['ex_link'], array('target' => '_blank'));
                                                    }
                                                ?>
                                                </div>
                                            </div>                                        
                                        </div>                                    
                                    </div>
                                </div>
                                <hr class="lineStyle">
                            </div><!--end meta--> 
                        </div>
                    </div>    
                    <?php endforeach; ?>
                    
                </div>
		<!--end post-->

		</div>
		<!--end blog column-->
		
            </div>
        </div>
    </div>
</div>
</div>