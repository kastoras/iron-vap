<div class="grid_16">
<!-- TABS START -->
    <div id="tabs">
         <div class="container">
            <ul>
                <li>
                    <?php
                    echo $this->Html->link('<span>Λίστα Εκδηλώσεων</span>',
                        array('controller' => 'Exibitions','action' => 'events'),
                        array('escape' => FALSE)
                    );
                    ?>
                </li>
                <li>
                    <?php
                    echo $this->Html->link('<span>Καινούρια Εκδήλωση</span>',
                        array('controller' => 'Exibitions','action' => 'newEvent'),
                        array('escape' => FALSE)
                    );
                    ?>
                </li> 
           </ul>
        </div>
    </div>
<!-- TABS END -->    
</div>

<!-- CONTENT START -->
<div class="grid_16" id="content">
    
    <h2 class="form_padding">Καινούρια Εκδήλωση</h2>
    
    <?php echo $this->Session->flash(); ?>
    <div class="portlet-content">
        
        <?php

            echo $this->Form->create();
            echo $this->Form->input('name',array('class'=>'smallInput wide'));
            echo $this->Form->input('place',array('class'=>'smallInput wide'));
            echo $this->Form->input('day_start',array('class'=>'smallInput wide'));
            echo $this->Form->input('day_close',array('class'=>'smallInput wide'));
            echo $this->Form->input('ex_link',array('class'=>'smallInput wide'));
            echo $this->Form->input('Image',array('class'=>'smallInput wide'));
            echo $this->Form->input('Info',array('class'=>'bigInput wide'));            
            echo $this->Form->submit('ΟΚ',array('class'=>'button'));
            echo $this->Form->end();

        ?>

    </div>
<!-- END CONTENT-->    
</div>