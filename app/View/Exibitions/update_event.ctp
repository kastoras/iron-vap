<div class="grid_16">
<!-- TABS START -->
    <div id="tabs">
         <div class="container">
            <ul>
                <li>
                    <?php
                    echo $this->Html->link('<span>Λίστα Εκδηλώσεων</span>',
                        array('controller' => 'Exibitions','action' => 'events'),
                        array('escape' => FALSE)
                    );
                    ?>
                </li>
                <li>
                    <?php
                    echo $this->Html->link('<span>Καινούρια Εκδήλωση</span>',
                        array('controller' => 'Exibitions','action' => 'newEvent'),
                        array('escape' => FALSE)
                    );
                    ?>
                </li> 
           </ul>
        </div>
    </div>
<!-- TABS END -->    
</div>
<!-- CONTENT START -->
<div class="grid_16" id="content">
    <h2 class="form_padding">Update Εκδήλωση</h2>
    
    <?php echo $this->Session->flash(); ?>
    <div class="portlet-content">
        
        <?php

            echo $this->Form->create();
            echo $this->Form->input('name',array('default'=>$event['Exibition']['name'],'class'=>'smallInput wide'));
            echo $this->Form->input('place',array('default'=>$event['Exibition']['place'],'class'=>'smallInput wide'));
            echo $this->Form->input('day_start',array('default'=>$event['Exibition']['day_start'],'class'=>'smallInput wide'));
            echo $this->Form->input('day_close',array('default'=>$event['Exibition']['day_close'],'class'=>'smallInput wide'));
            echo $this->Form->input('ex_link',array('default'=>$event['Exibition']['ex_link'],'class'=>'smallInput wide'));
            echo $this->Form->input('Image',array('default'=>$event['Exibition']['Image'],'class'=>'smallInput wide'));
            echo $this->Form->input('Info',array('default'=>$event['Exibition']['Info'],'class'=>'bigInput wide'));
            echo $this->Form->submit('Ανανέωση',array('class'=>'button'));
            echo $this->Form->end();

        ?>

    </div>
<!-- END CONTENT-->    
</div>