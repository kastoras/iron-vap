<!--page_container-->
<!--welcome-->
<div class=" welcome welcomeText">
    <div class="line_divider line_top">
    </div>
    <h1 class="greyColor">
        Πείτε μας την <?php echo $this->Html->link("ανάγκη",array('controller' => '/επικοινωνία')); ?> σας σε <?php echo $this->Html->link('Ατμοκαθαριστή','/ατμοκαθαριστές'); ?> και <?php echo $this->Html->link('Σκούπα','/σκούπες'); ?> για να σας προτείνουμε <br>το κατάλληλο προϊόν! |
        <img src="img/phone-ico.png" class="phoneIconMarginTop"/> <span class="sloganSpan1">2310 85 30 30</span>
    </h1>
    <div class="line_divider line_bottom">
    </div>
</div>
<!--//welcome-->
<div class="clear">
</div>


<div class="wrapper sliderMargin">
    <!--slider-->
    <ul id="sb-slider" class="sb-slider">
        <li>
            <img src="img/slider1.jpg" alt="image1"/>
            <?php echo $this->Html->link(
                '<div class="sb-description hidden-phone slider1">
                    <h3>Σκούπισμα και <br>Απολύμανση από ακάρεα.</h3>
                </div>',
                '/πολυσκούπες',
                array('escape' => false)); 
            ?>
        </li>
        <li>
            <img src="img/slider2.jpg" alt="image2"/>
            <?php echo $this->Html->link(
                '<div class="sb-description hidden-phone slider2">
                    <h3>Σκούπισμα Στέγνωμα Απολύμανση Συγχρόνως!</h3>
                </div>',
                '/unico-polti',
                array('escape' => false)); 
            ?>
        </li>
        <li>
            <img src="img/slider3.jpg" alt="image3"/>
            <?php echo $this->Html->link(
                '<div class="sb-description hidden-phone slider3">
                    <h3>Βαθύς καθαρισμός και απολύμανση<br>
                        ΜΟΝΟ με τη δύναμη του ατμού!</h3>
                </div>',
                '/ατμοκαθαριστές',
                array('escape' => false)); 
            ?>    
        </li>
    </ul>

    <div id="nav-arrows" class="nav-arrows">
        <a href="#">Next</a>
        <a href="#">Previous</a>
    </div>
    <div id="nav-dots" class="nav-dots">
        <span class="nav-dot-current"></span>
        <span></span>
        <span></span>
        <!--span></span>
        <span></span>
        <span></span>
        <span></span>-->
    </div>
</div>
<!-- /slider -->
<div class="clear">
</div>
<div class="row">
    <h3 class="pad15 categoriesHeader">Κατηγορίες προϊόντων</h3>
</div>

<div class="row">
    <div class="intro_sections">
        <div class="span4"> 
            <?php echo $this->Html->link(
                '<div class="big_icon">
                    <img src="img/category_home/vaporreto.jpg" />
                </div>
                <div class="link_title"><h2 class="link_title_overide">Ατμοκαθαριστές</h2></div>',
                '/ατμοκαθαριστές',
                array('escape' => false, 'class'=>'productsHome')); 
            ?>
        </div>
        <div class="span4">
            <?php echo $this->Html->link(
                '<div class="big_icon">
                    <img src="img/category_home/desinfector.jpg" />
                </div>
                <div class="link_title"><h2 class="link_title_overide">Απολύμανση</h2></div>','/οικολογική-απολύμανση',
                array('escape' => false, 'class'=>'productsHome'));
            ?>
        </div>
        <div class="span4">
            <?php echo $this->Html->link(
                '<div class="big_icon">
                    <img src="img/category_home/unico.jpg" />
                </div>
                <div class="link_title"><h2 class="link_title_overide">Unico</h2></div>','/unico-polti',
                array('escape' => false, 'class'=>'productsHome'));
            ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="intro_sections">        
        <div class="span4">
            <?php echo $this->Html->link(
                '<div class="big_icon">
                    <img src="img/category_home/lecoaspira.jpg" />
                </div>
                <div class="link_title"><h2 class="link_title_overide">Πολυσκούπες</h2></div>','/πολυσκούπες',
                array('escape' => false)); 
            ?>
                
        </div>
        <div class="span4">
            <?php echo $this->Html->link(
                '<div class="big_icon">
                    <img src="img/category_home/forzaspira.jpg" />
                </div>
                <div class="link_title"><h2 class="link_title_overide">Σκούπες</h2></div>','/σκούπες',
                array('escape' => false)); 
            ?>    
        </div>
        <div class="span4">
            <?php echo $this->Html->link(
                '<div class="big_icon">
                    <img src="img/category_home/iron.jpg" />
                </div>
                <div class="link_title"><h2 class="link_title_overide">Πρεσσοσίδερα</h2></div>','/πρεσσοσίδερα',
                array('escape' => false)); 
            ?>
        </div>        
    </div>
</div>

<div class="clear">
</div>
    
<!-- End Elastislide Carousel -->
