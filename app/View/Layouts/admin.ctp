<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Dashboard | Iron Vap</title>

    <?php
        echo $this->Html->css('admin/960');
        echo $this->Html->css('admin/reset');
        echo $this->Html->css('admin/text');
        echo $this->Html->css('admin/blue');
        echo $this->Html->css('admin/smoothness/ui');
        //echo $this->Html->css('admin/custom_styles');

        echo $this->Html->script('http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js');
        echo $this->Html->script('admin/blend/jquery.blend');
        echo $this->Html->script('admin/ui.core');
        echo $this->Html->script('admin/ui.sortable');
        echo $this->Html->script('admin/ui.dialog');
        echo $this->Html->script('admin/ui.datepicker');
        echo $this->Html->script('admin/effects');
        echo $this->Html->script('admin/flot/jquery.flot.pack');
        echo $this->Html->script('admin/graphs');    

        echo $this->Js->writeBuffer();
    ?>
    <!--[if IE]>
    <script language="javascript" type="text/javascript" src="js/flot/excanvas.pack.js"></script>
    <![endif]-->
	<!--[if IE 6]>
	<link rel="stylesheet" type="text/css" href="css/iefix.css" />
	<script src="js/pngfix.js"></script>
    <script>
        DD_belatedPNG.fix('#menu ul li a span span');
    </script>        
    <![endif]-->

</head>

<body>
    <!-- WRAPPER START -->
    <div class="container_16" id="wrapper">	
        <!--LOGO-->
        <div class="grid_8" id="logo">Iron Vap - Διαχείρηση</div>

        <!-- USER TOOLS END -->    
        <div class="grid_16" id="header">
            
            <!-- MENU START -->
            <div id="menu">
                <ul class="group" id="menu_group_main">
                    <li class="item first" id="two">
                        <?php
                            echo $this->Html->link(
                                '<span class="outer">
                                    <span class="inner content">
                                        Τιμές
                                    </span>
                                </span>',
                                array('controller' => 'Products','action' => 'prices'),
                                array('escape' => FALSE)
                            );
                        ?>
                    </li>
                    <li class="item last" id="three">
                        <?php
                            echo $this->Html->link(
                                '<span class="outer">
                                    <span class="inner reports png">
                                        Εκθέσεις
                                    </span>                              
                                </span>',
                                array('controller' => 'Exibitions','action' => 'events'),
                                array('escape' => FALSE)
                            );
                        ?>
                    </li>
                </ul>
            </div>
            <!-- MENU END -->
        </div> 
        <?php
            echo $this->fetch('content');
        ?>
        <div class="clear"> </div>
    </div>
    <!-- WRAPPER END -->
    <!-- FOOTER START -->
    <div class="container_16" id="footer">
        Website Administration Iron Vap
    </div>
<!-- FOOTER END -->
</body>
</html>
