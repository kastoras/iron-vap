<!DOCTYPE html>
<html lang="el" class="htmlCustom">
<head>
<meta charset="utf-8">
<title><?php echo $title_for_layout ?></title>
<meta name="description" content="<?php echo $description_for_layout ?>">
<meta http-equiv="content-language" content="el">

<!-- Open Graph data -->
<meta property="og:title" content="Εταιρεία εμπορίας ατμοκαθαριστών | σκουπών" />
<meta property="og:type" content="website" />
<meta property="og:url" content="http://www.iron-vap.gr/" />
<meta property="og:image" content="img/slider2.jpg" />
<meta property="og:description" content="Η εταιρία με έδρα τη Θεσσαλονίκη δραστηριοποιείτε στον χώρο των ατμοκαθαριστών, των σκουπών, των πρεσοσίδερων και των συσκευών απολύμανσης" />
<meta property="og:locale" content="el_GR" />

<link rel="shortcut icon" href="img/fav.jpg" type="image/jpg" />


<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,400italic,600,700' rel='stylesheet' type='text/css'>
<!--[if IE]>
	<link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet" type="text/css">
	<link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:400italic" rel="stylesheet" type="text/css">
	<link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:400italic" rel="stylesheet" type="text/css">
	<link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:600" rel="stylesheet" type="text/css">
	<link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:700" rel="stylesheet" type="text/css">
	<![endif]-->

<?php 
    echo $this->Html->css('prettyPhoto'); 
    echo $this->Html->css('bootstrap'); 
    echo $this->Html->css('font-awesome.min'); 
    echo $this->Html->css('theme'); 
    echo $this->Html->css('styles');
    echo $this->Html->css('styles_menu');
    
    echo $this->Html->css('max-width960');     
?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
<?php
    echo $this->Html->script('modernizr.custom.46884');
    
    echo $this->Html->script('jquery.easing.1.3');
    echo $this->Html->script('bootstrap.min');
    echo $this->Html->script('superfish');
    echo $this->Html->script('jquery.prettyPhoto');
    echo $this->Html->script('scripts');
    echo $this->Html->script('jquerypp.custom');
    echo $this->Html->script('jquery.elastislide');
    echo $this->Html->script('jquery.slicebox');
?>

<!--[if IE 7]>
<link rel="stylesheet" href="css/font-awesome-ie7.min.css">
<![endif]-->
<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
</head>
<body class="bodyCustom">
<div class="container containerBottom">
	<!--header-->
    <div class="header removeBorder">
        <div class="navbar navbar_ clearfix">
            <div class="row headerTop">
                    <!--logo-->
                <div class="span4 logosTopMargin">
                    <div class="logo">
                        <?php 
                            echo $this->Html->link(
                                $this->Html->image("/img/logo_narraw.png", array(
                                    "alt" => "Ατμοκαθαριστές Iron Vap",
                                    'class'=>'logoMargin'))
                                ,'/'
                                ,array('escape' => false)); 
                        ?>
                    </div>
                </div>
                <div class="span4"></div>
                <div class="span4 logosTopMargin">
                    <?php 
                        echo $this->Html->link(
                            $this->Html->image("/img/facebook_ico.png", array("alt" => "Ατμοκαθαριστές Iron Vap",'class'=>"fbIcon"))
                            ,'https://www.facebook.com/ironvapofficial'
                            ,array('escape' => false)); 
                    ?>        
                </div>                
            </div>
            <div class="row">
                <div class="span4">
                    <p class="greyColor welcomeMargine">
                        Καλώς ήλθατε! 
                    </p>
                </div>
                <div class="span8">
                        <!--menu-->
                    <nav id="main_menu">
                    <div id="cssmenu" class="menu_wrap topMenu">
                        <ul class="nav sf-menu">
                            <li><?php echo $this->Html->link('ΕΤΑΙΡΕΙΑ','/εταιρεία'); ?></li>    
                            <li class="has-sub"><a href="javascript:{}">ΠΡΟΪΟΝΤΑ</a>
                                <ul>
                                    <li><?php echo $this->Html->link('Ατμοκαθαριστές','/ατμοκαθαριστές'); ?></li>
                                    <li><?php echo $this->Html->link('Οικ. Απολύμανση | Κοριοί','/οικολογική-απολύμανση'); ?></li>
                                    <li><?php echo $this->Html->link('Πολυσκούπες','/πολυσκούπες'); ?></li>
                                    <li><?php echo $this->Html->link('UNICO POLTI','/unico-polti'); ?></li>
                                    <li><?php echo $this->Html->link('Σκούπες','/σκούπες'); ?></li>
                                    <li><?php echo $this->Html->link('Πρεσσοσίδερα','/πρεσσοσίδερα'); ?></li>
                                    <li><?php echo $this->Html->link('Σιδερώστρες','/σιδερώστρες'); ?></li>
                                </ul>
                            <li><?php echo $this->Html->link('ΕΚΘΕΣΕΙΣ','/εκθέσεις'); ?></li>                  
                            <li><?php echo $this->Html->link('SERVICE','/service'); ?></li>                            
                            <!--li><a href="">ΝΕΑ</a></li-->                  
                            <li><?php echo $this->Html->link('ΕΠΙΚΟΙΝΩΝΙΑ','/επικοινωνία'); ?></li>     
                        </ul>
                    </div>
                    </nav>
                </div>                
            </div>
        </div>
    </div>
    <!--//header-->
    <?php
        echo $this->fetch('content');
        echo $this->Session->flash();
    ?>
</div>
<!--//page_container-->
<!--footer-->
<div id="footer" class="footerCustom footerReset">
    <div class="wrap">
        <div class="container">
            <div class="row">

            </div>
        </div>
        </div>
        <div class="footer_bottom clearFooter">
        <div class="wrap">
            <div class="container">
                <div class="row">
                    <div class="span4"></div>
                    <div class="span4">
                        <div class="copyright">
                            <a href="">Iron Vap</a>
                            &copy;
                            <script type="text/javascript">
                                var d = new Date()
                                document.write(d.getFullYear())
                            </script>
                            - All Rights Reserved
                        </div>
                    </div>
                    <div class="span4"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--//footer-->

<?php
    echo $this->Html->script('templateScripts');
?>


</body>
</html>