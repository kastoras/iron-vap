<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Login | Profi Admin</title>

<?php
    echo $this->Html->css('admin/960');
    echo $this->Html->css('admin/reset');
    echo $this->Html->css('admin/text');
    echo $this->Html->css('admin/login');
?>
</head>

<body>
<div class="container_16">
    <?php
        echo $this->fetch('content');
    ?>

</div>
<br clear="all" />
</body>
</html>