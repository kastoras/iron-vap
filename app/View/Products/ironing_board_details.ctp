<div class="page_container">
    <h6><?php echo $this->Html->link("Αρχική",'/'); ?> / <?php echo $this->Html->link($category,'/'.$categoryLink); ?> / <?php echo $product["name"];?></h6>
		
    <div class="inner_content">
        <div class="row">
            <div class="row">
                <div class="post">
                    <div class="span6" >
                        <?php 
                            echo $this->Html->link(
                                $this->Html->image("/img/products/mid/".$product["photo"].".jpg", 
                                    array('alt' => $product["name"], "style" => "margin-top:14px;")) 
                                    . ' ' . __(''),
                                    '/img/products/'.$product["photo"].'.png',
                                    array('data-rel'=>'prettyPhoto[portfolio1]','escape' => false)
                                );
                        ?>                        
                    </div>
                    <div class="span6">   
                        <h1>
                            <span class="post_link product_header">
                                <?php echo $product["name"];?>
                            </span>
                        </h1>
                        <h3>
                            <blockquote>
                                <p><?php echo $product["header"];?></p>
                            </blockquote>
                        </h3>

                        <p class="justifiedText"><?php echo $product["product_text"];?></p>

                        <hr>
                    </div>
		</div>
            </div>
        </div>
    </div>
</div>
<hr>