<!-- CONTENT START -->
<div class="grid_16" id="content">
    <?php echo $this->Session->flash(); ?>
    <!--  TITLE START  --> 
    <div class="grid_9">
        <h1 class="dashboard">Διαχείριση Τιμών</h1>
    </div>
    <!--RIGHT TEXT/CALENDAR END-->
    <div class="clear">
    </div>
    <!--  TITLE END  -->    

    <!-- #PORTLETS START -->
    <div id="portlets">
        
    <div class="clear"></div>
    <!--THIS IS A WIDE PORTLET-->
    <div class="portlet">
        <div class="portlet-content nopadding">
            <form action="" method="post">
                <table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
                    <thead>
                    <tr>
                        <th width="20" scope="col">Α/Α</th>
                        <th width="136" scope="col">Προϊόν</th>
                        <th width="102" scope="col">Τιμή</th>
                        <th width="102" scope="col">Προσφορά</th>
                        <th width="129" scope="col">Ενέργεια</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php foreach($products as $key => $product): ?>
                        <tr>
                            <td width="20"><?php echo ($key+1).'.';?></td>
                            <td>
                            <?php
                                echo $product['Product']['name'];
                            ?>
                            </td>
                            <td>
                            <?php
                                echo $product['Product']['price'];
                            ?>    
                            </td>
                            <td>
                            <?php
                                echo $product['Product']['offer'];
                            ?>    
                            </td>                            
                            <td width="90">
                                <?php
                                    echo $this->Html->link('',
                                        array('controller' => 'Products','action' => 'updatePrice/'.$product['Product']['id']),
                                        array('class'=>'edit_icon','title'=>'Edit')
                                    );
                                ?>
                            </td>
                        </tr>
                        <?php endforeach ?>
                    </tbody>
                </table>
            </form>
        </div>
    </div>
        <!--  END #PORTLETS -->  
    </div>
</div>    
<div class="clear"> </div>
<!-- END CONTENT-->  