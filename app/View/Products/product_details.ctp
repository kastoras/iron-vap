<div class="page_container">
    <h6><?php echo $this->Html->link("Αρχική",'/'); ?> / <?php echo $this->Html->link($category,'/'.$categoryLink); ?> / <?php echo $product["name"];?></h6>
		
    <div class="inner_content">
        <div class="row">
            <div class="row">
                <div class="post postMarPad">
                    <div class="span6" >
                        <?php 
                            echo $this->Html->link(
                                $this->Html->image("/img/products/mid/".$product["photo"].".jpg", 
                                    array('alt' => $product["name"], "style" => "margin-top:14px;")) 
                                    . ' ' . __(''),
                                    '/img/products/'.$product["photo"].'.png',
                                    array('data-rel'=>'prettyPhoto[portfolio1]','escape' => false)
                                );
                        ?>                        
                    </div>
                    <div class="span6">   
                        <h1>
                            <span class="post_link product_header">
                                <?php echo $product["name"];?>
                            </span>
                        </h1>
                        <h3>
                            <blockquote>
                                <p><?php echo $product["header"];?></p>
                            </blockquote>
                        </h3>

                        <p class="justifiedText"><?php echo $product["product_text"];?></p>

                        <hr>
                    </div>
                        
                    <?php if($product["product_banners"]) : ?>                    
                    <div class="span6">
                        <?php
                            $banners = explode(",",$product["product_banners"]);
                            foreach ($banners as $banner){
                                echo $this->Html->image("/img/small_banners/".$banner.".jpg",array('class'=>'paddingRight5'));
                            }
                        ?>
                    </div>
                    <?php endif; ?>
                    
                    <div class="span6">
                        <h4>
                            <!--p><img src="img/phone-ico.png"/>Τηλεφωνικές Παραγγελείες: 2310 85 30 30</p-->
                            <?php echo $this->Html->image('phone-ico.png'); ?> Περισσότερες πληροφορίες: <span class="sloganSpan1">6946 124291</span>
                        </h4>
                    </div>                    
                    
                    <?php
                    
                        if($product["price"]){
                            if($product["offer"]){
                                echo $this->element('price',array('price'=>$product["price"],'offer'=>$product["offer"],'id'=>$product["id"]));
                            }
                            else{
                                echo $this->element('priceOnly',array('price'=>$product["price"],'id'=>$product["id"]));
                            }
                        }
                    ?>
                                     
		</div>
                
                <div class="span12">
                    <ul class="nav nav-tabs">
                        <li class="active"><a class="biggerFont" href="#advantages" data-toggle="tab">Πλεονεκτήματα</a></li>
                        <?php if($additives) : ?>
                        <li><a class="biggerFont" href="#uses" data-toggle="tab">Χρήσεις</a></li>
                        <?php endif; ?>
                        <?php if($product["accessorize"]) : ?>
                        <li><a href="#accesorize" data-toggle="tab" class="biggerFont">Αξεσουάρ</a></li>
                        <?php endif; ?>
                        <?php if($product["technical_features"]) : ?>
                        <li><a href="#technical" data-toggle="tab" class="biggerFont">Τεχνικά χαρακτηριστικά</a></li>
                        <?php endif; ?>
                        <?php if($product["video"]) : ?>
                        <li><a href="#video" data-toggle="tab" class="biggerFont">Βίντεο</a></li>
                        <?php endif; ?>                          
                        <?php if($product["disruption"]) : ?>
                        <li><a href="#disruption" data-toggle="tab" class="biggerFont">Διάσπαση</a></li>
                        <?php endif; ?>                        
                    </ul>
                    <div id="myTabContent" class="tab-content span12">
                        <div class="tab-pane fade in active" id="advantages">                            
                            <?php
                                $change_row = 1;
                                $loops = 4;
                                foreach ($advantages as $key => $advantage){
                                    if($change_row === 1){
                                        echo '<div class="row">';
                                    }
                                    echo '<div class="span4">';
                                    echo '<h4>
                                        <span class="post_link product_header">'
                                            .$advantage['product_advantages']['header'].
                                        '</span>
                                    </h4>
                                     <p class="justifiedText">';
                                    echo $this->Html->image("/img/products/product_advantages/".$advantage['product_advantages']['image'],
                                        array(
                                            'class'=>'paddingRight5',
                                            'alt'=>$advantage['product_advantages']['alt_text'],
                                            'align'=>'left'
                                        )
                                    );
                                    echo $advantage['product_advantages']["advantage_text"].'</p>';
                                    echo '</div>';
                                    if($change_row === 3){
                                        echo '</div>';
                                        $change_row =0;
                                    } 
                                    $change_row++;
                                    $loops = $key;
                                }
                                if($change_row !== 1 || $loops<2){
                                    echo '</div>';
                                }
                                
                                //echo '</div>';
                            ?>
                        </div>
                        <?php if($additives) : ?>
                        <div class="tab-pane fade" id="uses">
                            <?php
                                $change_row = 1;
                                foreach ($additives as $addive){
                                    if($change_row === 1){
                                        echo '<div class="row">';
                                    }
                                    echo '<div class="span6">';
                                    echo '<h3>
                                        <span class="post_link product_header">'
                                            .$addive['product_additives']['header'].
                                        '</span>
                                    </h2>';
                                    echo $this->Html->image("/img/products/product_addives/".$addive['product_additives']['image'],
                                        array(
                                            'alt'=>$addive['product_additives']['alt_text'],
                                            'class'=>'paddingDown5'
                                        )
                                    );
                                    echo ' <p>'.$addive['product_additives']["additive_text"].'</p>';
                                    echo '</div>';
                                    if($change_row === 2){
                                        $change_row =0;
                                        echo '</div>';
                                    } 
                                    $change_row++;
                                }
                                if($change_row === 2){
                                    echo '</div>';
                                }
                            ?>
                            <hr>
                        </div>
                        <?php endif; ?>
                        <?php if($product["accessorize"]) : ?>
                        <div class="tab-pane fade" id="accesorize">
                            <?php 
                                echo $this->Html->link(
                                    $this->Html->image("/img/products/accessorize/".$product["accessorize"].".jpg", 
                                        array('alt' => "Αξεσουάρ", "style" => "margin-top:14px;")) 
                                        . ' ' . __(''),
                                        "/img/products/accessorize/".$product["accessorize"].".jpg",
                                        array('data-rel'=>'prettyPhoto[portfolio1]','escape' => false)
                                    );
                            ?> 
                        </div>
                        <?php endif; ?>
                        <?php if($product["technical_features"]) : ?>
                        <div class="tab-pane fade" id="technical">
                            <div class="span8">
                                <table class="table table-bordered table-striped">
                                    <tbody>
                                        <?php $techFeatures = explode("_", $product["technical_features"]); ?>
                                        <?php foreach($techFeatures as $feature): ?>
                                        <?php $featureElements = explode("|",$feature);?>
                                        <tr>
                                            <td><?php echo $featureElements[0]; ?></td>
                                            <td><?php echo $featureElements[1]; ?></td>
                                        </tr>
                                        <?php endforeach ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <?php endif; ?>
                        <?php if($product["video"]) : ?>
                        <div class="tab-pane fade" id="video">
                            <div class="span8">
                                <div class="vendor">
                                    <object
                                        width="780" height="480"
                                        data="http://www.youtube.com/embed/<?php echo $product["video"];?>">
                                    </object>
                                </div>
                            </div>
                        </div>
                        <?php endif; ?>                        
                        <?php if($product["disruption"]) : ?>
                        <div class="tab-pane fade" id="disruption">
                            <div class="span8">

                                
                                <div class="span8"style="margin-top:14px;">
                                    <!-- carousel starts -->
                                    <div  id="portfolio_carousel" class="carousel slide">
                                        <div class="carousel-inner">
                                            <?php
                                                $slides = explode(",",$product["disruption"]);
                                                 foreach ($slides as $key => $slide):
                                            ?>
                                            
                                                
                                            
                                            <?php if($key == 0) : ?>
                                            <div class="item active">
                                                <?php echo $this->Html->image("/img/products/disruption/".$slide.".gif"); ?>
                                            </div>
                                            <?php endif; ?>
                                            <?php if($key != 0) : ?>
                                            <div class="item">
                                                <?php echo $this->Html->image("/img/products/disruption/".$slide.".gif"); ?>
                                            </div>
                                            <?php endif; ?>
                                            
                                            <?php endforeach ?>
                                        </div>
                                        <a class="left carousel-control" href="#portfolio_carousel" data-slide="prev"></a>
                                        <a class="right carousel-control" href="#portfolio_carousel" data-slide="next"></a>
                                    </div>
                                </div>

                                
                            </div>
                        </div>                            
                        <?php endif; ?>   
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<hr>