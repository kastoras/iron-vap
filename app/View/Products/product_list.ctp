<div class="page_container">
    <h6><?php echo $this->Html->link("Αρχική",'/'); ?> / <?php echo $category; ?></h6>
    <div class="inner_content">
        <div class="row">
            <!-- portfolio_block -->
            <div class="projects">        
                <?php foreach($products as $product): ?>
                <div class="span4 element category01">
                    <div class="hover_img">
                        <?php 
                        echo $this->Html->image("/img/products/thumbs/".$product['Product']['photo'].".png",
                            array("alt" => $product['Product']['name'])); 
                        ?>
                        <span class="portfolio_link">
                            <?php
                            echo $this->Html->link($product['Product']['name'],array('controller' => $product["Product"]["category"].'/'.$product['Product']['id']));
                            ?>
                        </span>
                    </div> 
                    <div class="item_description biggerFont paddingDown5">
                    <?php
                        echo $this->Html->link($product['Product']['name'],array('controller' => $product["Product"]["category"].'/'.$product['Product']['id']));
                    ?>
                       
                    </div>                
                    <span class="discreptionSpan"><?php echo $product['Product']['header']; ?></span>
                </div>
                <?php endforeach ?>

                <div class="clear"></div>
            </div>   
            <!-- //portfolio-->   
        </div>
    </div>
</div>