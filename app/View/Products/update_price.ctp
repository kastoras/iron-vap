<!-- CONTENT START -->
<div class="grid_16" id="content">
    <h2 class="form_padding">Αλλαγή τιμής: <?php echo $product['Product']['name']; ?></h2>
    <div class="portlet-content">
        
        <?php

            echo $this->Form->create();
            echo $this->Form->input('price',array('default'=>$product['Product']['price'],'class'=>'smallInput wide'));
            echo $this->Form->input('offer',array('default'=>$product['Product']['offer'],'class'=>'smallInput wide'));
            echo $this->Form->submit('Update Price',array('class'=>'button','div'=>array('class'=>'divClass form_padding')));
            echo $this->Form->end();

        ?>

    </div>
<!-- END CONTENT-->    
</div>